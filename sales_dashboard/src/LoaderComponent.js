import React from "react";

class Loader extends React.Component {
  render() {
    return <div className="lds-dual-ring" />;
  }
}

export default Loader;
