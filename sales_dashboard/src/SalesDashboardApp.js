import React, { Component } from "react";
import "./SalesDashboardApp.css";
import moment from "moment";
import Loader from "./LoaderComponent";

class SalesDashboardApp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [],
      sortDurationType: "today",
      sortRepresentativeType: "all",
      sortTopperType: "top",
      isLoading: false,
      funnelInformation: {
        total: 0,
        contactMade: 0,
        needsDefined: 0,
        proposalMade: 0,
        negotiationStarted: 0,
        won: 0
      },
      overviewInformation: {
        calls: 0,
        wins: 0,
        incrementedRevenue: 0
      },
      topSalesPerson: []
    };
    this.onSortDurationTypeClick = this.onSortDurationTypeClick.bind(this);
    this.onSortRepresentativeTypeChange = this.onSortRepresentativeTypeChange.bind(
      this
    );
    this.onSortTopperTypeClick = this.onSortTopperTypeClick.bind(this);
    this.getRelevantRecords = this.getRelevantRecords.bind(this);
    this.getTopSalesPerson = this.getTopSalesPerson.bind(this);
    this.getTopThree = this.getTopThree.bind(this);
  }

  onSortRepresentativeTypeChange(e) {
    let self = this;
    if (this.state.sortRepresentativeType === e.target.value) {
      return;
    }
    this.setState(
      {
        sortRepresentativeType: e.target.value
      },
      function() {
        let { sortDurationType, sortRepresentativeType } = this.state;
        let params = {
          sortDurationType,
          sortRepresentativeType,
          sortDirection: self.getSortType(self.state.sortTopperType)
        };
        self.getRelevantRecords(params);
        self.getTopSalesPerson(params);
      }
    );
  }

  getSortType(sortDirection) {
    return sortDirection !== "top";
  }

  getTopThree(items) {
    let temp = [];
    if (!items || !items.length) {
      return temp;
    }

    let len = items.length < 3 ? items.length : 3;
    for (let i = 0; i < len; i++) {
      temp.push(items[i]);
    }
    return temp;
  }

  onSortDurationTypeClick(type) {
    let self = this;
    if (this.state.sortDurationType === type) {
      return;
    }
    this.setState(
      {
        sortDurationType: type
      },
      function() {
        let { sortDurationType, sortRepresentativeType } = this.state;
        let params = {
          sortDurationType,
          sortRepresentativeType,
          sortDirection: self.getSortType(self.state.sortTopperType)
        };
        self.getRelevantRecords(params);
        self.getTopSalesPerson(params);
      }
    );
  }

  onSortTopperTypeClick(type) {
    let self = this;
    if (this.state.sortTopperType === type) {
      return;
    }
    this.setState(
      {
        sortTopperType: type
      },
      function() {
        let { sortDurationType, sortRepresentativeType } = this.state;

        let params = {
          sortDurationType,
          sortRepresentativeType,
          sortDirection: self.getSortType(self.state.sortTopperType)
        };
        self.getTopSalesPerson(params);
      }
    );
  }

  componentDidMount() {
    let { sortDurationType, sortRepresentativeType } = this.state;
    let params = {
      sortDurationType,
      sortRepresentativeType
    };
    this.getRelevantRecords(params);
    this.getTopSalesPerson(params);
  }

  getTopSalesPerson(params) {
    this.setState({
      isLoading: true
    });
    let self = this;
    fetch(
      "https://xoooc3gxzb.execute-api.ap-south-1.amazonaws.com/persontest/gettopsalesperson",
      {
        method: "POST",
        body: JSON.stringify(params),
        headers: {
          "Content-Type": "application/json"
        }
      }
    )
      .then(res => res.json())
      .then(response => {
        self.setState({
          isLoading: false
        });
        response.Items = response.Items || [];
        response.Items = this.getTopThree(response.Items);
        self.setState({
          topSalesPerson: response.Items
        });
      })
      .catch(error => {
        self.setState({
          isLoading: false
        });
        self.setState({
          topSalesPerson: []
        });
        console.log(error);
      });
  }

  getRelevantRecords(params) {
    this.setState({
      isLoading: true
    });
    let self = this;
    fetch(
      "https://a1qxazfa50.execute-api.ap-south-1.amazonaws.com/test/sales",
      {
        method: "POST",
        body: JSON.stringify(params),
        headers: {
          "Content-Type": "application/json"
        }
      }
    )
      .then(res => res.json())
      .then(response => {
        self.setState({
          isLoading: false
        });
        response.Items = response.Items || [];
        this.generateFunnelData(response.Items);
        self.setState({
          items: response.Items
        });
      })
      .catch(error => {
        self.setState({
          isLoading: false
        });
        self.setState({
          items: []
        });
        console.log(error);
      });
  }

  generateFunnelData(items) {
    let funnelInformation = this.state.funnelInformation;
    let overviewInformation = this.state.overviewInformation;
    funnelInformation.total = items.length;
    for (let i in items) {
      if (items[i].engagement_status === "Needs defined") {
        funnelInformation.needsDefined += 1;
      } else if (items[i].engagement_status === "Won") {
        funnelInformation.won += 1;
        overviewInformation.wins += 1;
      } else if (items[i].engagement_status === "Contact Made") {
        funnelInformation.contactMade += 1;
      } else if (items[i].engagement_status === "Proposal Made") {
        funnelInformation.proposalMade += 1;
      } else if (items[i].engagement_status === "Negotiations Started") {
        funnelInformation.negotiationStarted += 1;
      } else if (items[i].meeting_location === "Call") {
        overviewInformation.calls += 1;
      }
    }
    this.setState({
      funnelInformation
    });
  }
  render() {
    return (
      <div className="container">
        {this.state.isLoading ? <Loader /> : null}
        <div className="subContainer">
          <header className="header">
            <div className="logoImageParent">
              <img
                className="logoImg"
                src="./assets/logo.png"
                alt="Locusnine Logo"
              />
            </div>
            <span className="logoText">SALES DASHBOARD</span>
          </header>
          <div className="durationFilterContainer">
            <ul className="durationMenus">
              <li
                className={
                  "durationMenu " +
                  (this.state.sortDurationType === "today"
                    ? "activeDurationFilter"
                    : "")
                }
                onClick={() => this.onSortDurationTypeClick("today")}
              >
                <span>TODAY</span>
              </li>
              <li
                className={
                  "durationMenu " +
                  (this.state.sortDurationType === "last_week"
                    ? "activeDurationFilter"
                    : "")
                }
                onClick={() => this.onSortDurationTypeClick("last_week")}
              >
                <span>LAST WEEK</span>
              </li>
              <li
                className={
                  "durationMenu " +
                  (this.state.sortDurationType === "last_month"
                    ? "activeDurationFilter"
                    : "")
                }
                onClick={() => this.onSortDurationTypeClick("last_month")}
              >
                <span>LAST MONTH</span>
              </li>
              <li
                className={
                  "durationMenu " +
                  (this.state.sortDurationType === "this_quarter"
                    ? "activeDurationFilter"
                    : "")
                }
                onClick={() => this.onSortDurationTypeClick("this_quarter")}
              >
                <span>THIS QUARTER</span>
              </li>
              <li
                className={
                  "durationMenu " +
                  (this.state.sortDurationType === "this_year"
                    ? "activeDurationFilter"
                    : "")
                }
                onClick={() => this.onSortDurationTypeClick("this_year")}
              >
                <span>THIS YEAR</span>
              </li>
            </ul>
          </div>

          <div className="subFilterContainer">
            <div className="verticalFilterContainer">
              <select
                className="verticalFilterDropDown"
                value={this.state.sortRepresentativeType}
                onChange={e => this.onSortRepresentativeTypeChange(e)}
              >
                <option value="all">All Sales Representatives</option>
                <option value="Sales">Sales</option>
                <option value="Marketing">Marketing</option>
                <option value="Senior Management">Senior Management</option>
                <option value="Technology">Technology</option>
              </select>
            </div>
            <div className="topBottomFilterContainer">
              <ul className="durationMenus">
                <li
                  className={
                    "durationMenu width " +
                    (this.state.sortTopperType === "top"
                      ? "activeDurationFilter"
                      : "")
                  }
                  onClick={() => this.onSortTopperTypeClick("top")}
                >
                  <span>TOP</span>
                </li>
                <li
                  className={
                    "durationMenu width " +
                    (this.state.sortTopperType === "bottom"
                      ? "activeDurationFilter"
                      : "")
                  }
                  onClick={() => this.onSortTopperTypeClick("bottom")}
                >
                  <span>BOTTOM</span>
                </li>
              </ul>
            </div>
          </div>
          <div className="salesOverviewContainer">
            <div className="funnelAndWinsWrapper">
              <div className="informationFunnelContainer">
                <img className="funnel" src="./assets/funnel.png" />
                <div className="funnelInformation">
                  <div className="funnelSection">
                    <div className="funnelInformationWrapper">
                      <span className="title">Lead In</span>
                      <span className="fieldValue">
                        {this.state.funnelInformation.total}
                      </span>
                    </div>
                  </div>
                  <div className="funnelSection">
                    <div className="funnelInformationWrapper">
                      <span className="title">Contact Made</span>
                      <span className="fieldValue">
                        {this.state.funnelInformation.contactMade}
                      </span>
                    </div>
                  </div>
                  <div className="funnelSection">
                    <div className="funnelInformationWrapper">
                      <span className="title">Needs Defined</span>
                      <span className="fieldValue">
                        {this.state.funnelInformation.needsDefined}
                      </span>
                    </div>
                  </div>
                  <div className="funnelSection">
                    <div className="funnelInformationWrapper">
                      <span className="title">Proposal Made</span>
                      <span className="fieldValue">
                        {this.state.funnelInformation.proposalMade}
                      </span>
                    </div>
                  </div>
                  <div className="funnelSection">
                    <div className="funnelInformationWrapper">
                      <span className="title">Negotiations Started</span>
                      <span className="fieldValue">
                        {this.state.funnelInformation.negotiationStarted}
                      </span>
                    </div>
                  </div>
                  <div className="funnelSection">
                    <div className="funnelInformationWrapper">
                      <span className="titleBold">WON</span>
                      <span className="fieldValue">
                        {this.state.funnelInformation.won}
                      </span>
                    </div>
                  </div>
                </div>
              </div>
              <div className="callWinsRevenueContainer">
                <div className="cardInformationItem">
                  <div className="cardInformationItemContainer paddingCardItem">
                    <img className="callsIcon" src="./assets/ico-calls.png" />
                  </div>
                  <div className="cardInformationItemContainer">
                    <span className="title">Calls</span>
                    <span className="fieldValue">
                      {this.state.overviewInformation.calls}/
                      {this.state.funnelInformation.total}
                    </span>
                  </div>
                </div>
                <div className="cardInformationItem">
                  <div className="cardInformationItemContainer paddingCardItem">
                    <img className="callsIcon" src="./assets/ico-wins.png" />
                  </div>
                  <div className="cardInformationItemContainer">
                    <span className="title">WINS</span>
                    <span className="fieldValue">
                      {this.state.overviewInformation.wins}/
                      {this.state.funnelInformation.total}
                    </span>
                  </div>
                </div>
                <div className="cardInformationItem">
                  <div className="cardInformationItemContainer paddingCardItem">
                    <img className="callsIcon" src="./assets/ico-revenue.png" />
                  </div>
                  <div className="cardInformationItemContainer">
                    <span className="title">INCREMENTED REVENUE</span>
                    <span className="fieldValue">80k/100k</span>
                  </div>
                </div>
              </div>
            </div>
            <div className="salesTopperContainer">
              {console.log(this.state.topSalesPerson)}
              {this.state.topSalesPerson &&
              this.state.topSalesPerson.length > 0 ? (
                this.state.topSalesPerson.map(function(value, index) {
                  return (
                    <div className="cardInformationItemForMan" key={value.Id}>
                      <div className="cardWithManInBackground" />
                      <div className="cardWithManInBackgroundItem">
                        <span className="cardWithManName">
                          {value.sales_reprentative}
                        </span>
                        <div className="cardWithManContainer">
                          <span className="cardWithManLabel">MRR</span>
                          <span className="cardWithManValue">{value.mrr}</span>
                        </div>
                        <div className="cardWithManContainer">
                          <span className="cardWithManLabel">Logos</span>
                          <span className="cardWithManValue">
                            {value.logos}
                          </span>
                        </div>
                      </div>
                    </div>
                  );
                })
              ) : (
                <span className="colorWarning">No one is there yet.</span>
              )}
            </div>
          </div>
        </div>
        <div className="tableInformationContainer">
          <span className="tableHeader">Details</span>
          <table>
            <thead>
              <tr>
                <th>Sales Representative</th>
                <th>Date</th>
                <th>Client</th>
                <th>Primary Contact Person</th>
                <th>Title</th>
                <th>Vertical</th>
                <th>Meeting Location</th>
                <th>Purpose of Interaction</th>
                <th>Outcome and Remarks</th>
                <th>Engagement Status</th>
              </tr>
            </thead>
            <tbody>
              {this.state.items && this.state.items.length ? (
                this.state.items.map(function(item, index) {
                  return (
                    <tr
                      key={item.Id + "" + index}
                      className={
                        item.engagement_status === "Won"
                          ? "wonItem"
                          : item.engagement_status === "Lost"
                          ? "loseItem"
                          : ""
                      }
                    >
                      <td>{item.sales_reprentative}</td>
                      <td>{moment(item.date).format("LL")}</td>
                      <td>{item.client}</td>
                      <td>{item.primary_contact_person}</td>
                      <td>{item.title}</td>
                      <td>{item.vertical}</td>
                      <td>{item.meeting_location}</td>
                      <td>{item.purpose_of_interaction}</td>
                      <td>{item.outcome_and_remarks}</td>
                      <td>{item.engagement_status}</td>
                    </tr>
                  );
                })
              ) : (
                <tr className="noRecordFound">
                  <td className="colorWarning">No records found.</td>
                </tr>
              )}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

export default SalesDashboardApp;
