import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import SalesDashboardApp from "./SalesDashboardApp";
import * as serviceWorker from "./serviceWorker";

ReactDOM.render(<SalesDashboardApp />, document.getElementById("root"));
serviceWorker.unregister();
